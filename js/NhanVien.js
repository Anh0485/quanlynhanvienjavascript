
function NhanVien (_maNV, _hoTen, _email, _matKhau, _ngayDiLam, _chucVu){
    this.maNhanVien = _maNV;
    this.hoTen =_hoTen;
    this.email = _email;
    this.ngayDiLam = _ngayDiLam;
    this.chucVu = _chucVu;
    this.luongCoBan = 400;
    this.tongLuong = 0;
    
    
    this.tinhTongLuong = function () {
        if (this.chucVu === "Sếp" ){
            return this.tongLuong = this.luongCoBan*3;
        } else if (this.chucVu === "Trưởng phòng"){
            return this.tongLuong = this.luongCoBan*2;
        } else if (this.chucVu === "Nhân viên"){
            return this.tongLuong = this.luongCoBan;
        }
    }
            


}