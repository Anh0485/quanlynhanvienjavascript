
var mangNhanVien = [];
var validation = new Validation();



function themNhanVien() {
    var maNV = document.getElementById("msnv").value;
    var hoTenNV = document.getElementById("name").value;
    var emailNV = document.getElementById("email").value;
    var matKhau = document.getElementById("password").value;
    var ngayDiLam = document.getElementById("datepicker").value;
    var chucVu = document.getElementById("chucvu").value;

    var isValid = true;
    
    
    isValid &= validation.kiemTraRong(maNV,"tbMaNV","Mã nhân viên không được để trống") 
    && validation.kiemTraMaTrung(maNV,"tbMaNV","Mã nhân viên đã tồn tại",mangNhanVien);


   
    isValid &= validation.kiemTraRong(hoTenNV,"tbTen","Tên nhân viên không được để rỗng") &&
    validation.kiemTraKyTu(hoTenNV,"tbTen", "Họ tên không hợp lệ");
    
   

    isValid &= validation.kiemTraRong(emailNV,"tbEmail","Email không để trống") &&
    validation.kiemTraEmail(emailNV,"tbEmail","Email không hợp lệ");

   
    isValid &= validation.kiemTraRong(matKhau,"tbMatKhau","mk không để trống") &&
    validation.kiemTraDoDai(matKhau,"tbMatKhau","Mat khau phai dai 6-8 ký tự ",6,8);
    
    
    isValid &= validation.kiemTraChucVu("chucvu","tbChucVu","Yêu cầu chọn chức vụ");

    console.log(isValid);

    if (isValid){
       
    var nhanVien = new NhanVien(maNV, hoTenNV, emailNV, matKhau, ngayDiLam, chucVu);
    nhanVien.tinhTongLuong();

        
    mangNhanVien.push(nhanVien);
    HienThi(mangNhanVien);
    }



    
}
function HienThi(mangHienThi) {
    var content = "";
    for (var i = 0; i < mangHienThi.length; i++) {
        var nhanVien = mangHienThi[i];
        content += `
            <tr>
            <td>${nhanVien.maNhanVien}</td>
            <td>${nhanVien.hoTen}</td>
            <td>${nhanVien.email}</td>
           
            <td>${nhanVien.ngayDiLam}</td>
            <td>${nhanVien.chucVu}</td>
            <td>${nhanVien.tongLuong}</td>
            <td>
            <button class = "btn btn-danger" data-id="${nhanVien.maNhanVien}"
            onclick="XoaNhanVien(event)">Xóa</button></td>
            <button class = "btn btn-danger" data-id="${nhanVien.maNhanVien}"
            onclick="LoadThongTinNV(event)" data-target="#myModal" data-toggle="modal">Sửa</button></td>

            </tr>
            `
    }
    var tableDanhSach = document.getElementById("tableDanhSach");
    tableDanhSach.innerHTML = content;

}



function LuuLS() { 
    var jsonData = JSON.stringify(mangNhanVien);
   


    
    localStorage.setItem("DSNV", jsonData);

}

function LayLS() {
    var jsonData = localStorage.getItem("DSNV");

   
    if(jsonData !== null){
    
    mangNhanVien = JSON.parse(jsonData);
    console.log(mangNhanVien);
    HienThi(mangNhanVien);
    }

    
}







function LoadThongTinNV(e) {
   
    var btn = e.target;
    var id = btn.getAttribute("data-id");
    console.log(id);
    
    var index = TimViTri(id);
    console.log(index);
    var nhanVien = mangNhanVien[index];
    console.log(nhanVien);
   

    document.getElementById("msnv").value = nhanVien.maNhanVien;
    document.getElementById("name").value = nhanVien.hoTen;
    document.getElementById("email").value = nhanVien.email;
    document.getElementById("password").value = nhanVien.matKhau;
    document.getElementById("datepicker").value = nhanVien.ngayDiLam;
    document.getElementById("chucvu").value = nhanVien.chucVu;

    document.getElementById("msnv").disabled = true;
    document.getElementById("btnThemNV").style.display = "none";
    document.getElementById("btnCapNhat").style.display = "block";
}





function CapNhatNV() {
    
    var maNV = document.getElementById("msnv").value;
    var hoTenNV = document.getElementById("name").value;
    var emailNV = document.getElementById("email").value;
    var matKhau = document.getElementById("password").value;
    var ngayDiLam = document.getElementById("datepicker").value;
    var chucVu = document.getElementById("chucvu").value;

    
    var nhanVienMoi = new NhanVien(maNV, hoTenNV, emailNV, matKhau, ngayDiLam,chucVu);
    nhanVienMoi.tinhTongLuong();
    var index = TimViTri(maNV);
    mangNhanVien[index] = nhanVienMoi;
    HienThi(mangNhanVien);
}



 function timKiemNV(){
     var mangNVTimThay = [];
    var keyword = document.getElementById("searchName").value;
   
    
      for ( var i= 0; i < mangNhanVien.length; i++){
       var nhanVien = mangNhanVien[i];
       
       keyword = keyword.toLowerCase().replace(/\s/g, '');
       
       
       if(nhanVien.hoTen.toLowerCase().replace(/\s/g, '').indexOf(keyword) !== -1){
           mangNVTimThay.push(mangNhanVien[i]);
       }
    }
    if (mangNVTimThay.length !== 0){
        HienThi(mangNVTimThay);
    }

    if(mangNVTimThay.length !== 0){
        HienThi(mangNVTimThay)
    } else{
        alert("Không tìm thấy");
    }

 }



function TimViTri(id) {
    for (var i = 0; i < mangNhanVien.length; i++) {


        if (mangNhanVien[i].maNhanVien === id) {
            return i;
        }


    }
    return -1;
}



function XoaNhanVien(e) {
    var btn = e.target;
    var id = btn.getAttribute("data-id");
    var index = TimViTri(id);
    mangNhanVien.splice(index, 1);
    HienThi(mangNhanVien);
}
LayLS();








var btnThemNV = document.getElementById("btnThemNV");
btnThemNV.addEventListener("click", themNhanVien); // callback function


var btnThem = document.getElementById("btnThem");
btnThem.addEventListener("click", function(){
    document.getElementById("msnv").disabled = false;
    document.getElementById("btnThemNV").style.display = "block";
    document.getElementById("btnCapNhat").style.display = "none";
});


var btnCapNhat = document.getElementById("btnCapNhat");
btnCapNhat.addEventListener("click",CapNhatNV);


var btnTimNV = document.getElementById("btnTimNV");
btnTimNV.addEventListener("click",timKiemNV);

var inputSearchName = document.getElementById("searchName");
inputSearchName.addEventListener("keyup", function (e){
    if( e.keyCode === 13){
        timKiemNV();
    }
});

var btnLuuLS = document.getElementById("btnLuuLS");
btnLuuLS.addEventListener("click", LuuLS);


var btnLayLS = document.getElementById("btnLayLS");
btnLayLS.addEventListener("click", LayLS);


